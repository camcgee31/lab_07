#!/bin/bash

# Hange man game made by Chase McGee
#the first part of the code is just the asscii functions that are echod to the user for grafics. they also take in underline array that displays each number the geuss right
nofail() 
{
echo " ______________"
echo "    |/      |  "
echo "    |          "
echo "    |          "
echo "    |          "
echo "    |          "
echo "    |          "
echo "____|___       "

echo ""
echo "word: $@"

}

failed1()
{
echo " ______________"
echo "    |/      |  "
echo "    |      (_) "
echo "    |          "
echo "    |          "
echo "    |          "
echo "    |          "
echo "____|___       "

echo ""
echo "word: $@"
}

failed2()
{
echo " ______________"
echo "    |/      |  "
echo "    |      (_) "
echo "    |       |  "
echo "    |          "
echo "    |          "
echo "    |          "
echo "____|___       "

echo ""
echo "word: $@"
}
failed3()
{
echo " ______________"
echo "    |/      |  "
echo "    |      (_) "
echo "    |      \|  "
echo "    |          "
echo "    |          "
echo "    |          "
echo "____|___       "

echo ""
echo "word: $@"
}
failed4()
{
echo " ______________"
echo "    |/      |  "
echo "    |      (_) "
echo "    |      \|/ "
echo "    |          "
echo "    |          "
echo "    |          "
echo "____|___       "

echo ""
echo "word: $@"
}
failed5()
{
echo " ______________"
echo "    |/      |  "
echo "    |      (_) "
echo "    |      \|/ "
echo "    |       |  "
echo "    |          "
echo "    |          "
echo "____|___       "

echo ""
echo "word: $@"
}
failed6()
{
echo " ______________"
echo "    |/      |  "
echo "    |      (_) "
echo "    |      \|/ "
echo "    |       |  "
echo "    |        \ "
echo "    |          "
echo "____|___       "

echo ""
echo "word: $@"
}
allfailed()
{
echo " ______________"
echo "    |/      |  "
echo "    |      (_) "
echo "    |      \|/ "
echo "    |       |  "
echo "    |      / \ "
echo "    |          "
echo "____|___       "

echo ""
echo "word: $@"
# if this function is ran then you lose and to exit the game
echo "You lose"
exit 0
}
# set failed count to 0 this varible will be used to count the amout of times the user failes to guess the right letter. 
failedCount=0

echo "1:) Random word"
echo "2:) User input word"
echo "type number for choice"
read num
# ask the user what word to
if [ "$num" = "1" ]
then
	# I forgot this part in my first submit. This simply reads from the file and puts all the lines into a array and radomly picks from one
	arrword=()
	file=$(cat words.txt | tr -d "[:blank:]")
	for line in $file
	do
			arrword+=("$line")
	done
	
	lengthoffile=${#arrword[@]}
	randomnum=$(( $RANDOM % $lengthoffile + 1 ))
	word=${arrword[$randomnum]}
elif [ "$num" = "2" ]
then
	echo "Input word to use for game"
	read word
	clear
fi
# get the charater could of the word this will be used to make the underlinearray
wordcound=$(echo -n "$word" | wc -c)

# the underline array that is made here is used for the graffics to show users what letters they guesed right and how long the word is
#First we get the word count and make a new array based of of the lenth of the word and insurt a _ for each charater
c=0
while [ $c -lt $wordcound ]
do 
	underlinearray+=("_")
	((c++))
done

#set totalfound to 0. this will used to determin if the user wins. 
totalfound=0
#set a while loop to run forever
while [ true ]
do
	#set found to 0. found is used if it is > then 0 then that means at least one charater was guesed. This will be used to deturmin if the charater was in the word and prevent failedcount from incressing
	#It resets after each guess. If it is 0 after guessing that means the user failed to guess the charater and to ++ to failedcount
	found=0
	# For the graffics check failedcount then run the function deticated to its value. also underlinearray is inputed into the function to allow
	# that function to echo it
	if [ $failedCount -eq 0 ]
	then
		nofail ${underlinearray[@]}
	elif [ $failedCount -eq 1 ]
	then
		failed1 ${underlinearray[@]}
	elif [ $failedCount -eq 2 ]
	then
		failed2 ${underlinearray[@]}
	elif [ $failedCount -eq 3 ]
	then
		failed3 ${underlinearray[@]}
	elif [ $failedCount -eq 4 ]
	then
		failed4 ${underlinearray[@]}
	elif [ $failedCount -eq 5 ]
	then
		failed5 ${underlinearray[@]}
	elif [ $failedCount -eq 6 ]
	then
		failed6 ${underlinearray[@]}
	elif [ $failedCount -eq 7 ]
	then
		allfailed ${underlinearray[@]}
	fi
	#recive user input for the guess letter
	echo "Guess a letter"
	read letter
	# take the word and seperate each letter into its own elment in arrword
	arrword=($(echo $word|sed  's/\(.\)/\1 /g'))
	# if the user has fand a total of the orignal word count that means they found each letter and they win and exit the script.
	if [ $totalfound -eq $wordcound ]
	then
		echo "You win"
		exit 0
	fi
	
	# positionolet is used to keep trace of the postion of each letter. so the dog is 3 letters by the time the loop gets to g the value should be 2
	# It is used to to change the underlinearray so if the user gueses the correct value it knows the psotion then changes the value to the letter gussed.
	positionolet=0
	# loop throgh the charters in the word
	for i in "${arrword[@]}"
	do 
		# if the used letter is in the array then ++ found to prevent the next if statment and ++ totalfound to say that they have found so ment letters. Again that is used
		# later to determin if you win
		if [ "$i" = "$letter" ]
		then
			((found++))
			((totalfound++))
			echo "$letter"
			underlinearray[$positionolet]="$letter"
		fi
		((positionolet++))
	done
	
	# if found = 0 so the letter guesed was never found in the array then echo letter not in word and failed count ++.
	if [ $found -eq 0 ]
	then
		echo "Letter not in word"
		((failedCount++))
	fi
	# if totalfound = wordcound then the player wins since they found all words
	if [ $totalfound -eq $wordcound ]
	then
		echo "You win"
		echo "The hidden word was $word"
		exit 0
	fi
	# clear the terminal screen to keep it clean.s		
	clear

done

